# LIFX-HTTP

This is a thin wrapper of the [Lifx HTTP protocol](http://api.developer.lifx.com/).

This library is not, in any way, affiliated or related to Lifi Labs, Inc.. Use at your own risk.
